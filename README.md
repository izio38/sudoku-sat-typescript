# sudoku-sat-typescript
A simple, lightweight sudoku SAT-modeling using [rsolve](https://github.com/xgillard/rsolve), a SAT-solver written in
rust by Xavier Gillard.

## Installation
sudoku-sat-typescript will not be release under npm, hence, you need to run it from source files.

1) ensure Node.js `v10+` is installed. (node -v)
2) download source files.
3) open up a terminal, place yourself under source file root directory and run ``npm i``, il will install dependencies.
4) run ``npm run serve``, it will run the server side.
5) open a browser and go to ``http://localhost:8080/``.
Note: to change port, ``src/server/config.json``

## Usage
![alt text](https://i.gyazo.com/0933ef124e8c06d7f84e697991268678.png "Fig-1")

### Fill the grid
You are able to fill the grid with digits within [1..9] only.

### Find solution
When your instance is as desired, click the ``Find Solution`` button, it will try to solve the instance.
If there is a solution, ``Fill Solutions`` and ``Show Solution`` will show up!

Else, only ``Show Generation`` will show up.

### Fill Solutions
If there is solution, by clicking on the ``Fill solutions`` button, the grid will be filled with the model given by rsolve.

### Show Generation
Open a new tab with the generated DIMACS file.

### Show Solution
Open a new tab with the proposed model given by rsolve.