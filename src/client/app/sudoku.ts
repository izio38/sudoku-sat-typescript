import {Row, Line, Column} from './types';
import {
    OneNumberIn19InEachCell,
    exactlyOneNumberIn19InEachColumn,
    exactlyOneNumberIn19InEachRow,
    exactlyOneNumberIn19InEachSubgrid
} from './constraints';
import {occurrences} from "./utils";


export class Grid {
    public rows: Array<Line> = [];
    public columns: Array<Line> = [];

    constructor() {
    }

    init() {
        this.rows = [];
        this.columns = [];
        for (let i = 0; i < 9; i++) {
            this.rows.push([]);
            this.columns.push([]);
            for (let j = 0; j < 9; j++) {
                this.rows[i].push(-1);
                this.columns[i].push(-1);
            }
        }
    }
}

export class Sudoku {
    public grid: Grid = new Grid();
    public events: Array<Event> = [];

    constructor() {
    }

    // Init all grid cells with -1
    init(cb: Function) {
        this.grid.init();
        cb();
    }

    update(col: number, row: number, val: number) {
        this.grid.columns[col][row] = val;
        this.grid.rows[row][col] = val;
    }

    refreshDOM(col: number, row: number, val: number) {
        let el = document.querySelector(`.cell[col="${col}"].cell[row="${row}"]`);
        if (el === null) throw new Error('[ERROR]: el null in refresh()');

        if (val !== -1) {
            el.textContent = `${val}`
        } else {
            el.textContent = "";
        }
    }

    refreshAllDOM() {
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9; j++) {
                this.refreshDOM(i, j, this.grid.rows[i][j]);
            }
        }
    }

    generateConstraints() {
        let str = OneNumberIn19InEachCell("", this.grid);
        str = exactlyOneNumberIn19InEachColumn(str, this.grid);
        str = exactlyOneNumberIn19InEachRow(str, this.grid);
        str = exactlyOneNumberIn19InEachSubgrid(str, this.grid);

        // count clauses number
        let _occ = occurrences(str, "\n", false);
        // DIMACS header
        let _s = "p cnf 999 " + _occ + "\n";
        // set the header before clauses
        // more on DIMACS format: https://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html
        _s = _s.concat(str);

        return _s;

    }

    // TODO: difficulty level
    generate() {

    }
}