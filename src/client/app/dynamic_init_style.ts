export function initGridStyle() {
    // Set border styles:
    let left = document.querySelectorAll(`.cell[col="0"]`);
    let right = document.querySelectorAll(`.cell[col="8"]`);
    let bot = document.querySelectorAll(`.cell[row="8"]`);
    let top = document.querySelectorAll(`.cell[row="0"]`);

    left.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderLeftColor"] = 'black';
    });

    // inside col borders:
    left = document.querySelectorAll(`.cell[col="3"]`);
    left.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderLeftColor"] = 'black';
    });
    left = document.querySelectorAll(`.cell[col="6"]`);
    left.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderLeftColor"] = 'black';
    });

    right.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderRightColor"] = 'black';
    });

    // inside col borders:
    right = document.querySelectorAll(`.cell[col="2"]`);
    right.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderRightColor"] = 'black';
    });
    right = document.querySelectorAll(`.cell[col="5"]`);
    right.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderRightColor"] = 'black';
    });

    top.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderTopColor"] = 'black';
    });
    top = document.querySelectorAll(`.cell[row="3"]`);
    top.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderTopColor"] = 'black';
    });
    top = document.querySelectorAll(`.cell[row="6"]`);
    top.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderTopColor"] = 'black';
    });
    bot.forEach((c) => {
        let anchor = c as HTMLAnchorElement;
        anchor.style["borderBottomColor"] = 'black';
    });
}