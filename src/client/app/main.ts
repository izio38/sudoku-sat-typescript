/* 
    Client entry point
*/

// Sudoku class
import {Sudoku} from './sudoku';
// dynamic grid and sub-grid styling
import {initGridStyle} from './dynamic_init_style'
// we just use $.ajax function since the native fetch() was acting weirdly
// Jquery documentation: https://api.jquery.com/
import * as $ from "jquery"
// clauses generation
import {
    OneNumberIn19InEachCell,
    exactlyOneNumberIn19InEachColumn,
    exactlyOneNumberIn19InEachRow,
    exactlyOneNumberIn19InEachSubgrid
} from './constraints';
// substring occurences of a string
import {occurrences} from './utils';

let config = require('../config.json');

let selectedHTMLElement: HTMLElement;
let currentInput: HTMLElement;
let isEditing = false;
let contentLoaded = false;
let currentSolution: Array<Object>;
let currentFile: string = "";
let isSolution: boolean = false;
let sudoku = new Sudoku();

document.addEventListener("DOMContentLoaded", function () {
    contentLoaded = true;

    document.onkeypress = (k: KeyboardEvent) => {
        if ((k.key == 'Escape') && isEditing) {
            isEditing = false;
            currentInput.remove();
            selectedHTMLElement.style['display'] = 'block';
        }
    }
});

function initSudoku() {
    sudoku.init(() => {
        // sometimes DOMContent (basically HTML view) is loaded after the js files
        if (contentLoaded) {
            initGrid();
        } else {
            document.addEventListener("DOMContentLoaded", function () {
                initGrid();
            });
        }
    })
}

initSudoku();

// dynamically switch the selected cell to an input (so the user can start typing)
function swapInput(el: HTMLElement) {
    isEditing = true;
    let parent = el.parentNode;
    let input = document.createElement("INPUT");
    input.id = 'edit-value';
    currentInput = input;

    input.onkeypress = (k: KeyboardEvent) => {

        // user validate the input value
        if (k.key == 'Enter') {

            isEditing = false;
            let val: string = (<HTMLInputElement>document.getElementById('edit-value')!).value;
            let parsedInt: number = parseInt(val);

            // parse the number:
            // parseInt: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/parseInt
            if ((!isNaN(parsedInt) && parsedInt <= 9 && parsedInt >= 1) || (val == "")) {
                el.textContent = val;
                let colNumber: number = parseInt(el.getAttribute("col")!);
                let rowNumber: number = parseInt(el.getAttribute("row")!);

                // if the value is nothing
                if (val == "") {
                    // reset the cell
                    sudoku.update(colNumber, rowNumber, -1);
                } else {
                    sudoku.update(colNumber, rowNumber, parsedInt);
                }

                isSolution = false;

                // set back the cell
                input.remove();
                el.style["display"] = "block";
            } else {
                // if it's not a number | valid number
                input.remove();
                el.style["display"] = "block";
            }
        }
    }

    input.className += 'edit-cell';
    input.setAttribute('type', 'text');

    if (parent === null) throw new Error('[ERROR]: parent is null.');
    el.style['display'] = 'none';
    parent.insertBefore(input, el);
    // set the typing focus on the input
    input.focus();

}

function initClickEvent() {
    let cellsArray = document.querySelectorAll(".cell");
    // for each cell
    cellsArray.forEach((c) => {
        // listen to the mouse click event
        c.addEventListener("click", () => {
            // if the user was editing a cell, just ignore
            if (isEditing) return;
            selectedHTMLElement = c as HTMLElement;
            swapInput(selectedHTMLElement);
        });
    });
}

function initButtonEvent() {
    let find_button = (document.querySelector("#find-solution")! as HTMLElement);
    let fill_button = (document.querySelector("#fill-solution")! as HTMLElement);
    let r_button = (document.querySelector("#reset-grid")! as HTMLElement);
    let generation_button = (document.querySelector("#download-generation")! as HTMLElement);
    let solution_button = (document.querySelector("#download-solution")! as HTMLElement);
    if (!find_button || !fill_button || !r_button || !solution_button || !generation_button) throw new Error("[ERROR] Button is null.");

    // user clicked on the find button
    find_button.onclick = () => {
        // generate all clauses
        let _s = sudoku.generateConstraints();
        // send to NodeJs server
        askMinisat(_s);
    }
    fill_button.onclick = () => {
        if (isSolution) {
            console.log(currentSolution)
            currentSolution.forEach((c: any) => {
                sudoku.update(c.column, c.row, c.number);
            });
            // render the view
            sudoku.refreshAllDOM();
        } else {
            // No solution init found;
        }
    }
    r_button.onclick = () => {
        if (isEditing) {
            currentInput.remove();
            selectedHTMLElement.style["display"] = "block";
        }
        isEditing = false;
        isSolution = false;

        setButtonState(false);

        initSudoku();
    }
    generation_button.onclick = () => {
        if (isSolution) {
            openInNewTab(`${config.BASE_URL}/generations/${currentFile}`);
        }
    }

    solution_button.onclick = () => {
        if (isSolution) {
            openInNewTab(`${config.BASE_URL}/solutions/${currentFile}`);
        }
    }
}

function openInNewTab(url: string) {
    var win = window.open(url, '_blank');
    if (!win) throw new Error("[Error] Win is null");
    win.focus();
}

function askMinisat(val: string) {
    // do a POST request to /api/sat
    // with http://api.jquery.com/jquery.ajax/
    $.ajax({
        type: "POST",
        url: `${config.BASE_URL}/api/sat/`,
        data: "clauses=" + encodeURIComponent(val),
        success: (data) => {
            if (data == 500) {
                console.error("Internal server error.");
                setButtonState(false);
            } else if (!data.success) {
                console.error("UNSAT"); // TODO: also add message to UI
                setButtonState(false);
                showGenerationButton();
                currentFile = data.file;
                isSolution = true;
            } else if (data.success) {
                console.log(data);
                let solution: Array<Object> = [];

                data.model.forEach((l: any) => {
                    solution.push({
                        column: parseInt(l[0]) - 1,
                        row: parseInt(l[1]) - 1,
                        number: parseInt(l[2])
                    })
                });

                currentFile = data.file;
                currentSolution = solution;
                isSolution = true;
                setButtonState(true);
            }
        },
        error: (data) => {
            console.error(data);
        }
    })
}

function showGenerationButton() {
    let generation_button = (document.querySelector("#download-generation"));
    if (generation_button) {
        generation_button.removeAttribute("disabled");
    }
}

function setButtonState(val: boolean) {
    let fill_button = (document.querySelector("#fill-solution"));
    let generation_button = (document.querySelector("#download-generation"));
    let solution_button = (document.querySelector("#download-solution"));
    if (!fill_button || !generation_button || !solution_button) throw new Error("[ERROR] button is null");
    if (val) {
        fill_button.removeAttribute("disabled");
        generation_button.removeAttribute("disabled");
        solution_button.removeAttribute("disabled");
    } else {
        fill_button.setAttribute("disabled", "disabled");
        generation_button.setAttribute("disabled", "disabled");
        solution_button.setAttribute("disabled", "disabled");
    }
}

function initGrid() {
    sudoku.refreshAllDOM();

    let container = document.getElementById("container");
    if (container === null) throw new Error("[ERROR] container is null.");
    initGridStyle();
    // Initit clicks event for cells:
    initClickEvent();
    // Initit buttons event for cells:
    initButtonEvent();

    // Set main container as visible
    // Why now? Because before, nothing was initialized
    container.style["display"] = 'flex';
}
