export type Row = Array<Line>;
export type Column = Array<Line>;
export type Line = Array<number>;