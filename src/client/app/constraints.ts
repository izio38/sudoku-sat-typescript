import {Grid} from './sudoku';

export function OneNumberIn19InEachCell(str: string, grid: Grid): string {
    let tmp = "";
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            for (let n = 0; n < 9; n++) {
                // if the grid is initialized at this point:
                if (grid.rows[i][j] != -1) {
                    tmp = tmp + `${toBase9(i, j, (grid.rows[i][j] - 1))} `;
                    break;
                }
                // else just set all possible values (1,2,...,9)
                tmp = tmp + `${toBase9(i, j, n)} `;
            }
            str = addClauseToStr(tmp, str);
            tmp = "";
        }
    }
    return str
}

export function exactlyOneNumberIn19InEachRow(str: string, grid: Grid): string {
    for (let j = 0; j < 9; j++) {
        for (let n = 0; n < 9; n++) {
            for (let i1 = 0; i1 < 9; i1++) {
                for (let i2 = i1 + 1; i2 < 9; i2++) {
                    let tmp = `-${toBase9(i1, j, n)} -${toBase9(i2, j, n)} `;
                    str = addClauseToStr(tmp, str);
                }
            }
        }
    }
    return str
}

export function exactlyOneNumberIn19InEachColumn(str: string, grid: Grid): string {
    for (let i = 0; i < 9; i++) {
        for (let n = 0; n < 9; n++) {
            for (let j1 = 0; j1 < 9; j1++) {
                for (let j2 = j1 + 1; j2 < 9; j2++) {
                    let tmp = `-${toBase9(i, j1, n)} -${toBase9(i, j2, n)} `;
                    str = addClauseToStr(tmp, str);
                }
            }
        }
    }
    return str
}

export function exactlyOneNumberIn19InEachSubgrid(str: string, grid: Grid): string {
    // for each possible values in {0, ..., 8}
    for (let n = 0; n < 9; n++) {
        // for each sub grid by row:
        for (let _i = 0; _i < 3; _i++) {
            // for each sub grid by column:
            for (let _j = 0; _j < 3; _j++) {
                // for each cell in the subgrid(i, j) along the current row:
                for (let iSelector = 0; iSelector < 3; iSelector++) {
                    // for each cell in the subgrid(i, j) along the current column:
                    for (let jSelector = 0; jSelector < 3; jSelector++) {

                        for (let k = jSelector + 1; k < 9; k++) {
                            let a = _i * 3 + iSelector;
                            let b = _j * 3 + jSelector;
                            let c = _j * 3 + (k % 3);

                            // TODO: be human readable
                            if (Math.floor(((Math.floor(k / 3) + a) / (3 * (_i + 1)))) >= 1) {
                                break;
                            }

                            let tmp = `-${toBase9(a, b, n)} -${toBase9(Math.floor(k / 3) + a, c, n)} `;
                            str = addClauseToStr(tmp, str);
                        }

                    }
                }
            }
        }
    }
    return str
}

// i: row, j: columns, n: number
export function toBase9(i: number, j: number, n: number) {
    return (((i + 1) * 100) + ((j + 1) * 10) + (n + 1));
}

export function toNormalBase(n: string) {
    return {_i: n[0], _j: n[1], _n: n[2]};
}

export function addClauseToStr(clause: string, str: string) {
    return (str += (clause + `0\n`));
}