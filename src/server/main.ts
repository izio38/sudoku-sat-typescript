/* 
    Server entry point
*/

// Express Framework: https://www.npmjs.com/package/express
import express from "express";
// https://nodejs.org/docs/latest/api/path.html
// To support path joining on both Windows and Linux.
import path from "path";
// https://www.npmjs.com/package/fs-extra
// native fs (file system) module plus some extra:
import fsExtra from 'fs-extra'
// https://nodejs.org/api/child_process.html
import {exec} from "child_process"
// https://www.npmjs.com/package/body-parser
// To parse HTTP request bodies
let bodyParser = require('body-parser');

let config = require('./config.json');

// Create an Express app
const app = express();
const isWin = process.platform === "win32";

// Set server port
app.set('port', config.PORT);

// Some usefull path
let basePath = config.STATIC_DIR.split('/');
basePath.push('generations');
let genPath = path.join.apply(null, basePath);
basePath[basePath.length - 1] = 'solutions';
let solPath = path.join.apply(null, basePath);
basePath = config.SERVER_DIR.split('/');
basePath.push(`rsolve${isWin ? '.exe' : ''}`);
let rSolvePath = path.join.apply(null, basePath);

// CHMOD 755 on rsolve:
if (!isWin) exec(`chmod 755 ${rSolvePath}`);

// To accept clauses request, default limit is too tiny
app.use(bodyParser({limit: "50mb"}));
// Serve the static directory to users
app.use(
    express.static(path.join(__dirname, "../", "static"))
);

// fetch the next file number
async function getNextNumber() {
    let ex = fsExtra.existsSync(`${config.STATIC_DIR}/generations`);
    if (!ex) {
        await fsExtra.mkdir(`${config.STATIC_DIR}/generations`);
        return 0;
    }
    return await fsExtra.readdir(`${config.STATIC_DIR}/generations`).then((files) => {
        return (1 + Math.max.apply(null, files.map((val, i) => parseInt(val.split('.txt')[0]))));
        // directory possibly not created.
    }).catch((err) => {
        console.error(err)
    });
}

// Listen /api/sat/ as a POST request: https://en.wikipedia.org/wiki/POST_(HTTP)
app.post("/api/sat/", async (req, res, next) => {
    // If the request contains clauses
    if (req.body && req.body.clauses) {
        // fetch the next file number
        let nextNumber = await getNextNumber();
        let pathToFile = path.join(genPath, `${nextNumber}.txt`);
        // create a file (since writeFile doesn't create it by default)
        fsExtra.createFile(pathToFile).then(() => {
            // write the given clauses to it
            fsExtra.writeFile(pathToFile, req.body.clauses).then(() => {
                // exec a shell command which execute rSolve (rust solver) with the previous created file
                //    -p to print a model if there is one
                let _process = exec(`${rSolvePath} ${genPath}/${nextNumber}.txt -p`,
                    async (err, stdout, stderr) => {
                        // Error handling
                        if (err) {
                            // Send there's an internal server error
                            // All http code : https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP
                            res.sendStatus(500);
                            // throw keywork documentation: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/throw
                            throw err;
                        }
                        // if it's UNSAT:
                        if (stdout.indexOf('UNSATISFIABLE') != -1) {
                            // .json() allow to send a JSON
                            // res object documentation: http://expressjs.com/fr/api.html#res
                            // JSON documentation: https://www.json.org/
                            return res.json({success: false, file: `${nextNumber}.txt`});
                        }
                        // there is a model, send it to the actual client
                        stdout = stdout.split('\nv ')[1].split(' 0\nc ')[0];

                        // save model
                        // remove the literals with '-'.
                        let model = stdout.split(' ').filter((val, i) => val[0] != '-');
                        let modelPath = path.join(solPath, `${nextNumber}.txt`);
                        await saveModel(stdout, modelPath);

                        res.json({success: true, file: `${nextNumber}.txt`, model: model});
                    });
                // Error handling:
            }).catch((err) => {
                console.error(err);
                res.sendStatus(500);
            });
        }).catch((err) => {
            console.log(err);
            res.sendStatus(500);
        });
    } else {
        res.sendStatus(409);
    }
});

function saveModel(model: string, file: string) {
    return new Promise(async (res, rej) => {

        let ex = fsExtra.existsSync(`${config.STATIC_DIR}/solutions`);
        if (!ex) {
            await fsExtra.mkdir(`${config.STATIC_DIR}/solutions`);
        }

        fsExtra.createFile(file).then(() => {
            fsExtra.writeFile(file, model).then(() => {
                res();
            }).catch((err) => rej(err));
        }).catch((err) => rej(err));

    })
}

// Start server
const server = app.listen(app.get('port'), () => {
    console.log(`server stated on port ${app.get('port')}`);
});