/* 
    Gulp is a library that allows us to manage tasks.
    tasks could be: build, server, clean and deploy (hidden)...
    Instead of repeat some actions manually we might want to do it automatically.
    Documentation on Gulp: https://gulpjs.com/docs/en/getting-started/quick-start
*/
var gulp = require("gulp");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var tsify = require("tsify");
var sftp = require('gulp-sftp-up4');
var paths = {
    static: ['src/client/*.html', 'src/client/*.css'],
    server: ['src/server/rsolve', 'src/server/rsolve.exe', 'src/server/config.json'],
    deploy: ['dist/**/*', 'package.json', 'dist/server/config.json']
};
var ts = require("gulp-typescript");
var tsProject = ts.createProject("./src/server/tsconfig.json");
const fsExtra = require("fs-extra");
const gutil = require("gulp-util");

const {fork} = require("child_process");
const fs = require("fs-extra");
let g_process = null;

function copy() {
    return new Promise((res, rej) => {
        gulp.src(paths.static)
            .pipe(gulp.dest("dist/static/"))
            .on('end', () => {
                gulp.src(paths.server)
                    .pipe(gulp.dest("dist/server/"))
                    .on('end', res)
                    .on('error', rej);
            })
            .on('error', rej);
    });
}

async function buildClient() {
    return new Promise((res, rej) => {
        browserify({
            basedir: '.',
            debug: true,
            entries: ['src/client/app/main.ts'],
            cache: {},
            packageCache: {}
        })
            .plugin(tsify)
            .bundle()
            .pipe(source('bundle.js'))
            .pipe(gulp.dest("dist/static"))
            .on('end', res)
            .on('error', rej);
    });
}

async function buildServer() {
    return new Promise((res, rej) => {
        tsProject.src()
            .pipe(tsProject())
            .js.pipe(gulp.dest("dist/server"))
            .on('end', res)
            .on('error', rej);
    });
}

function reload() {
    killProcess();
    launchProcess();
}

async function copyAndReload() {
    await copy();
    reload();
}

function killProcess() {
    g_process.kill();
}

async function launchProcess() {
    return await new Promise((res, rej) => {
        console.log("try to launch process");
        g_process = fork('./dist/server/main.js');

        g_process.on('error', (err) => {
            console.error("[ERROR]: ", err);
        });

        g_process.on('close', (e) => {
            console.log("Node process closed.");
        });
        res();
    });
}

function cleanDist(cb) {
    fs.remove('dist/', () => {
        cb()
    });
}

gulp.task("serve", async function () {

    await new Promise((res, rej) => {

        cleanDist(async () => {
            await copy();
            buildClient().then(() => {

                buildServer().then(async () => {
                    gulp.watch("src/client/*.html").on("change", copyAndReload);
                    gulp.watch("src/client/*.css").on("change", copyAndReload);
                    gulp.watch("src/client/app/*.ts").on("change", async () => {
                        await buildClient();
                        reload();
                    });
                    gulp.watch("src/server/*.ts").on("change", async () => {
                        await buildServer();
                        reload();
                    });
                    gulp.watch("src/*.css").on("change", copyAndReload);

                    launchProcess().then(() => res()).catch((err) => rej(err));

                }).catch((err) => rej(err));

            }).catch((err) => rej(err));
        })
    });

});

function useAppropriateConfig() {
    let env = gutil.env.env || 'development';
    if (env === 'prod') {
        let file = require('./src/client/config.json');
        file.BASE_URL = "http://izio.fr:8080";
        fsExtra.writeFileSync('./src/client/config.json', JSON.stringify(file, null, 2));

        file = require('./src/server/config.json');
        file.STATIC_DIR = "static";
        file.SERVER_DIR = "server";
        fsExtra.writeFileSync('./src/server/config.json', JSON.stringify(file, null, 2));
    } else {
        let file = require('./src/client/config.json');
        file.BASE_URL = "http://localhost:8080";
        fsExtra.writeFileSync('./src/client/config.json', JSON.stringify(file, null, 2));

        file = require('./src/server/config.json');
        file.STATIC_DIR = "dist/static";
        file.SERVER_DIR = "dist/server";
        fsExtra.writeFileSync('./src/server/config.json', JSON.stringify(file, null, 2));
    }
}

function build() {
    return new Promise((res, rej) => {
        cleanDist(async () => {
            useAppropriateConfig();
            await copy();
            buildClient().then(() => {

                buildServer().then(() => {
                    res();
                }).catch((err) => rej(err));

            }).catch((err) => rej(err));
        })
    });
}

gulp.task("build", async function () {
    await build();
});


// unused
function deploy() {
    return new Promise((res, rej) => {
        gulp.src(paths.deploy)
            .pipe(sftp({
                host: '5.135.180.207',
                user: 'root',
                pass: 'hidden',
                remotePath: '/home/izio.fr/sudoku'
            }))
            .on('end', res)
            .on('error', rej);
    });
}

gulp.task("deploy", async function () {
    await build();

    return await deploy();
});